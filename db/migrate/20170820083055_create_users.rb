class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.integer :age
      t.string :gender
      t.string :favorite_colors, :array => true
      t.integer :level, null: false, default: 1

      t.timestamps
    end
  end
end
