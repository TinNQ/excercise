class UsersController < ApplicationController
	before_action :set_user, only: [:edit, :update]

	def new
		@user = User.new
	end

	def edit
	end

	def create
		@user = User.new(params_user)
		@user.save
	end

	def update
		@user.update(params_user)
	end

	private
	def params_user
		params.require(:user).permit(:name, :email, :age, :gender, {:favorite_colors => []}, :level)
	end

	def set_user
		@user = User.find(params[:id])
	end
end
